# This class implements an animation, whose frames are specified by a filename pattern and whose speed is variable.
class sonickit.Animation
	# Creates a frame sequence for use in an animation. The pattern should be of the form path/to/$.ext, where $ will be replaced by a number from 1 to numFrames.
	@genFrameSequence: (pattern, numFrames) ->
		return (pattern.replace('$', String(i)) for i in [1..numFrames])
	
	# interval is the amount of time the animation will wait until it advances to the next frame.
	constructor: (@frames, @interval) ->
		@currentFrame = 0
		@timeUntilNextFrame = @interval
		
	getCurrentImage: ->
		sonickit.Resources.getImage(@frames[@currentFrame])
	
	update: ->
		if --@timeUntilNextFrame is 0
			@timeUntilNextFrame = @interval
			@currentFrame = (@currentFrame + 1) % @frames.length
	
	draw: (ctx, x, y) ->
		ctx.drawImage @getCurrentImage(), x, y