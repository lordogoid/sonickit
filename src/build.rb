#!/usr/bin/env ruby

require 'fileutils'

files = Dir.glob '**/*.coffee'
FIRST = %w[sonickit.coffee moves.coffee animation.coffee players.coffee]
files -= FIRST
files.insert 0, *FIRST

PREFIX = '../tmp/'

OUT = '../lib/sonickit.js'

t = false

outlist = []

files.each do |file|
	outlist << (cfile = PREFIX + file.sub(/\.coffee$/, '.js'))
	FileUtils.mkdir_p (dir = File.dirname(cfile))
	if !File.exist?(cfile) || File.mtime(file) > File.mtime(cfile)
		puts file
		system "coffee -co #{dir} #{file}"
		t = true
	end
end

system "cat #{outlist.join ' '} > #{OUT}" if t