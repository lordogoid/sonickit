class sonickit.Camera
	constructor: ({@width, @height, @leftXBorder, @rightXBorder, @topYBorder, @bottomYBorder}) ->
		@x = 0
		@y = 0
		# The Y position where the player should tend to appear when on ground
		@playerYTarget = @height / 2

	# The maximum number of pixels the camera is allowed to shift in one frame
	maxShift: 16
	
	savePos: ->
		@oldX = @x
		@oldY = @y
	
	# Returns the apparent position of the camera.
	getInterpolatedRect: (remTime) ->
		x: @oldX * (1 - remTime) + @x * remTime
		y: @oldY * (1 - remTime) + @y * remTime
		width: @width
		height: @height
	
	# Moves the camera to where the player is, unless the player is moving too fast.
	shift: (player) ->
		relX = player.x - @x
		relY = player.y - @y
		# Adjust the X position
		if relX > @rightXBorder
			@x += Math.min @maxShift, relX - @rightXBorder
		else if relX < @leftXBorder
			@x -= Math.min @maxShift, @leftXBorder - relX
		# Adjust the Y position
		if player.airborne
			if relY > @bottomYBorder
				@y += Math.min @maxShift, relY - @bottomYBorder
			else if relY < @topYBorder
				@y -= Math.min @maxShift, @topYBorder - relY
		else
			yTarget = @playerYTarget
			if player.spinning or player.crouching
				yTarget += player.rollHeightDiff / 2 * Math.cos(player.angle)
			diff = relY - yTarget
			if Math.abs(diff) > 0.1
				if diff > 0
					@y += Math.min @maxShift, diff
				else
					@y -= Math.min @maxShift, -diff

