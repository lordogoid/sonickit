class sonickit.CollisionMask
	constructor: (img) ->
		canvas = document.createElement 'canvas'
		@width = img.width
		@height = img.height
		canvas.width = @width
		canvas.height = @height
		ctx = canvas.getContext '2d'
		ctx.drawImage img, 0, 0
		@data = ctx.getImageData 0, 0, @width, @height
	
	isPixelSolid: (x, y) ->
		return no unless 0 <= x < @width and 0 <= y < @height
		@data.data[(y * @width + x) * 4 + 3] > 192