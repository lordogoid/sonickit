class sonickit.Keyboard
	# The keycodes for the arrow keys and space. Keycodes should not be needed for anything else.
	@KCODES =
		32: 'Space'
		37: 'Left'
		38: 'Up'
		39: 'Right'
		40: 'Down'

	constructor: (canvas) ->
		# This table records the number of frames for which each key has been pressed, or
		# undefined if that key is not pressed.
		@keyTable = {}
		# This table records the keys that have been pressed and released between two frames,
		# so that such keypresses are not lost
		@delAfterNextFrame = {}
		# Make the canvas keyboard-focusable
		canvas.tabIndex = 0
		canvas.addEventListener 'keydown', (event) => @keyDown(@convertEventToKeyName event)
		canvas.addEventListener 'keyup', (event) => @keyUp(@convertEventToKeyName event)
		
	# Converts the information contained in a key event to a key name.
	convertEventToKeyName: (event) ->
		# If there is an entry in the KCODES translation table for this key, use it. Otherwise,
		# it's probably an alphabetic char. Anything else is irrelevant.
		@constructor.KCODES[event.keyCode] or event.keyChar
	
	keyDown: (key) ->
		# When this handler is running, there is no frame being drawn; the value will be
		# incremented when the next frame runs.
		@keyTable[key] ?= 0
		delete @delAfterNextFrame[key]
	
	keyUp: (key) ->
		if @keyTable[key] is 0
			@delAfterNextFrame[key] = yes
		else
			delete @keyTable[key]
	
	# Increments the recorded time of all currently-pressed keys. This is invoked once per frame.
	update: ->
		for key of @keyTable
			@keyTable[key]++
		null
	
	# Deletes from the key table all keys that were pressed (but not held) before that frame.
	delPressedKeys: ->
		for key of @delAfterNextFrame
			delete @keyTable[key]