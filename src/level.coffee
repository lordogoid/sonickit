sonickit.min = (a, b) ->
	if not a?
		b
	else if not b?
		a
	else
		Math.min a, b

class sonickit.Level
	constructor: (@width, @height) ->
		@grid = new sonickit.ObjectGrid
		@camera = new sonickit.Camera(width: 320, height: 224, leftXBorder: 152, rightXBorder: 168, \
		                              topYBorder: 64, bottomYBorder: 128)
	
	# Adds an object to the level (placing it in the correct collision groups).
	add: (obj) ->
		obj.level = this
		@grid.add obj
		
	# Inserts the given player object on this level.
	placePlayer: (@player) ->
		@player.level = this
		player.setPos @spawnPositions[@player.constructor.name] or @spawnPositions.default
		@player.setInitialState()
		@resetPlayerStats()
		
	# Resets the player's stats upon entering the level. (by default only the rings are zeroed)
	resetPlayerStats: ->
		@player.ringCount = 0
	
	# Draws this level onto the given 2D graphics context.
	# ctx: the graphics context to draw onto
	# remTime: the time remaining until the next physics frame, divided by the
	#          PHYSICS_INTERVAL.
	draw: (ctx, remTime) ->
		ctx.clearRect 0, 0, ctx.canvas.width, ctx.canvas.height
		cam = @camera.getInterpolatedRect remTime
		for layer, i in @grid.grid when layer?
			# Draw the player behind other objects on the same layer
			if @player.layer is i
				@player.draw ctx, cam, remTime
			@grid.forEachObj cam, i, (obj) =>
				obj.draw ctx, cam, remTime
		return
	
	update: ->
		@player.savePos()
		@camera.savePos()
		@handleCollision()
		@player.update()
		@player.calcIntPos()
		@camera.shift @player
	
	handleCollision: ->
		# The heights found by the A, B, C, D, and horizontal sensors
		ah = bh = ch = dh = eh = fh = gh = hh = ih = jh = kh = lh = null
		sensor = @player.quadrant.sensors
		min = sonickit.min
		@grid.forEachObj @player.getRect(), @player.layer, (obj) =>
			unless obj.ignorePhysics
				# Set all sensor values that have not been measured before
				if obj.topSolid
					ah ?= nah = sensor.A @player, obj
					bh ?= nbh = sensor.B @player, obj
				# If the player is on the ground, ignore the C and D sensors
				if @player.airborne and obj.bottomSolid
					ch ?= nch = sensor.C @player, obj
					dh ?= ndh = sensor.D @player, obj
				if obj.rightSolid
					eh ?= neh = sensor.E @player, obj
					if @player.airborne
						ih ?= nih = sensor.I @player, obj
						jh ?= njh = sensor.J @player, obj
					else
						fh ?= nfh = sensor.F @player, obj
				if obj.leftSolid
					gh ?= ngh = sensor.G @player, obj
					if @player.airborne
						kh ?= nkh = sensor.K @player, obj
						lh ?= nlh = sensor.L @player, obj
					else
						hh ?= nhh = sensor.H @player, obj
				# If the sensors detected this object, have the player determine if there actually has
				# been a collision. (This may not be the case, for example at the start of a jump)
				if (nah? or nbh?) and @player.shouldHitFloor(obj, min(nah, nbh))
					obj.onFloorCollision @player
				if (nch? or ndh?) and @player.shouldHitCeiling(obj, min(nch, ndh))
					obj.onCeilingCollision @player
				if (neh? or nfh?) and @player.shouldHitLeftWall(obj, min(neh, nfh))
					obj.onLeftWallCollision @player
				if (ngh? or nhh?) and @player.shouldHitRightWall(obj, min(ngh, nhh))
					obj.onRightWallCollision @player
			else
				obj.onCollision @player
		# Now use the values to orient the player
		@player.collide ah, bh, ch, dh, eh, fh, gh, hh, ih, jh, kh, lh
