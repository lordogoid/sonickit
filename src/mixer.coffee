# This class implements methods for background music and sound effect playing,
# including transitions.
class sonickit.Mixer
	SILENCE =
		volume: 1
		play: -> return
		pause: -> return
	
	constructor: ->
		@currentBGM = SILENCE
		@nextBGM = SILENCE
	
	setBackgroundMusic: (music) ->
		@currentBGM.pause()
		@currentBGM = music
		music.play()
		
	playSoundEffect: (effect) ->
		new Audio(effect.src).play()