# Returns true if the two given rectangles (any objects with x, y, width and height properties)
# intersect.
sonickit.rectintersect = (r1, r2) ->
	Math.max(r1.x, r2.x) < Math.min(r1.x + r1.width, r2.x + r2.width) and
	Math.max(r1.y, r2.y) < Math.min(r1.y + r1.height, r2.y + r2.height)

class sonickit.Object
	constructor: (@x, @y, @layer) ->
		# Set the default animation and the current frame
		# The collision, if any, is stored as a property of the class
		@anim = @defaultAnim
		@animFrame = 0
		img = @currentImage()
		@width = img.width
		@height = img.height
		
	# Returns the image that currently represents this object.	
	currentImage: ->
		sonickit.Resources.getImage @anim[@animFrame]
	
	# Draws this object onto the given graphics context, relative to the given camera.
	draw: (ctx, camera) ->
		ctx.drawImage @currentImage(), @x - camera.x, @y - camera.y
		
	# Renders this object's collision mask if that has not been done already.
	getCollisionMask: ->
		@constructor::collisionMask ?= new sonickit.CollisionMask sonickit.Resources.getImage @collisionMaskName
	
	# Returns the distance between the point (x, y) and the surface of this object, along a vertical line at most n pixels long.
	# The line's direction is determined by adding (dx, dy) to the current (x, y) at each step.
	# If the surface of this object does not intersect the line, null will be returned.
	pixelDistance: (x, y, n, dx, dy) ->
		x -= @x
		y -= @y
		d = 0
		mask = @getCollisionMask()
		for _ in [0...n] by 1
			if mask.isPixelSolid x, y
				return d
			else
				d++
				x += dx
				y += dy
		null
	
	# Returns the distance between the point (x, y) and the surface of this object, along a vertical line at most n pixels long.
	verticalDistance: (x, y, n, dy) ->
		if @collisionMaskName?
			@pixelDistance x, y, n, 0, dy
		else
			d = if dy > 0 then @y - y else y - @y
			if d < n and @x <= x < @x + @width then d else null
	
	# Returns the distance between the point (x, y) and the surface of this object, along a horizontal line at most n pixels long.
	horizontalDistance: (x, y, n, dx) ->
		if @collisionMaskName?
			@pixelDistance x, y, n, dx, 0
		else
			d = if dx > 0 then @x - x else x - @x
			if d < n and @y <= y < @y + width then d else null
			
	vd: @::verticalDistance
	hd: @::horizontalDistance
	
	onFloorCollision: (player) ->
	onCeilingCollision: (player) ->
	onLeftWallCollision: (player) ->
	onRightWallCollision: (player) ->
	onCollision: (player) ->