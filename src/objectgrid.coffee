# A modulus operation whose result is positive for positive divisors.
sonickit.mod = (x, y) ->
	x - y * Math.floor(x / y)

# This class is used for grouping objects according to their playfield coordinates, to gain quick access to the objects
# residing in any given area and layer.
class sonickit.ObjectGrid
	# The default width and height of the grid blocks.
	blockWidth: 256
	blockHeight: 256
	
	constructor: ->
		@grid = []
	
	# Adds the given object to this grid.
	add: (obj) ->
		gridLayer = @grid[obj.layer] ?= []
		a = Math.floor(obj.x / @blockWidth)
		b = Math.floor(obj.y / @blockHeight)
		# Pretend to expand the object to the upper-left corner of its upper-left block, to ensure objects that span
		# grid boundaries are placed in all the groups they should.
		mod = sonickit.mod
		for i in [a..a + Math.floor((obj.width + mod(obj.x, @blockWidth)) / @blockWidth)] by 1
			for j in [b..b + Math.floor((obj.height + mod(obj.y, @blockHeight)) / @blockHeight)] by 1
				gridLayer[j] ?= []
				gridLayer[j][i] ?= []
				gridLayer[j][i].push obj
		return

	# Calls the given function once for each object contained within the given rectangle.
	# Only objects on the specified layer are searched.
	forEachObj: (rect, layer, func) ->
		gridLayer = @grid[layer]
		mod = sonickit.mod
		a = Math.floor(rect.x / @blockWidth)
		b = Math.floor(rect.y / @blockHeight)
		for i in [a..a + Math.floor((rect.width + mod(rect.x, @blockWidth)) / @blockWidth)] by 1
			for j in [b..b + Math.floor((rect.height + mod(rect.y, @blockHeight)) / @blockHeight)] by 1
				group = gridLayer?[j]?[i]
				if group?
					for obj in group
						func(obj) if sonickit.rectintersect(obj, rect)
		return
