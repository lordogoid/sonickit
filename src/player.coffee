sonickit.sign = (x) ->
	if x < 0 then -1 else 1
	
frameseq = sonickit.Animation.genFrameSequence

class sonickit.Player
	# The relative angle at which the player switches quadrants
	QUADRANT_SWITCH_THRESHOLD: Math.PI / 4
	QUADRANT_SWITCH_ADJUSTMENT: Math.PI / 2
	
	ctl = sonickit.controls
	
	width: 20
	height: 40
	# How much the player's height decreases when rolling
	rollHeightDiff: 10
	
	# The horizontal and vertical sensor lengths
	hl: 11
	vl: 40
	
	# The horizontal sensor offset
	ho: 4
	
	# The four physics quadrants (see the Sonic Physics Guide for details).
	# The base values are actually the quantity that must be added to the measured angle to give the real angle.
	# p = player
	# o = object
	FLOOR:
		base: 0
		left: 'RIGHT_WALL'
		right: 'LEFT_WALL'
		sensors:
			A: (p, o) -> o.vd p.fx - p.hw, p.fy, p.vl, 1
			B: (p, o) -> o.vd p.fx + p.hw, p.fy, p.vl, 1
			E: (p, o) -> o.hd p.fx, p.fy - p.ho, p.hl, 1
			F: (p, o) -> o.hd p.fx, p.fy + p.ho, p.hl, 1
			G: (p, o) -> o.hd p.fx, p.fy - p.ho, p.hl, -1
			H: (p, o) -> o.hd p.fx, p.fy + p.ho, p.hl, -1
	LEFT_WALL:
		base: Math.PI / 2
		left: 'FLOOR'
		right: 'CEILING'
		sensors:
			A: (p, o) -> o.hd p.fx, p.fy - p.hw, p.vl, -1
			B: (p, o) -> o.hd p.fx, p.fy + p.hw, p.vl, -1
			E: (p, o) -> o.vd p.fx + p.ho, p.fy, p.hl, 1
			F: (p, o) -> o.vd p.fx - p.ho, p.fy, p.hl, 1
			G: (p, o) -> o.vd p.fx + p.ho, p.fy, p.hl, -1
			H: (p, o) -> o.vd p.fx - p.ho, p.fy, p.hl, -1
	CEILING:
		base: Math.PI
		left: 'LEFT_WALL'
		right: 'RIGHT_WALL'
		sensors:
			A: (p, o) -> o.vd p.fx + p.hw, p.fy, p.vl, -1
			B: (p, o) -> o.vd p.fx - p.hw, p.fy, p.vl, -1
			E: (p, o) -> o.hd p.fx, p.fy + p.ho, p.hl, -1
			F: (p, o) -> o.hd p.fx, p.fy - p.ho, p.hl, -1
			G: (p, o) -> o.hd p.fx, p.fy + p.ho, p.hl, 1
			H: (p, o) -> o.hd p.fx, p.fy - p.ho, p.hl, 1
	RIGHT_WALL:
		base: -Math.PI / 2
		left: 'CEILING'
		right: 'FLOOR'
		sensors:
			A: (p, o) -> o.hd p.fx, p.fy + p.hw, p.vl, 1
			B: (p, o) -> o.hd p.fx, p.fy - p.hw, p.vl, 1
			E: (p, o) -> o.vd p.fx - p.ho, p.fy, p.hl, -1
			F: (p, o) -> o.vd p.fx + p.ho, p.fy, p.hl, -1
			G: (p, o) -> o.vd p.fx - p.ho, p.fy, p.hl, 1
			H: (p, o) -> o.vd p.fx + p.ho, p.fy, p.hl, 1
	AIR:
		sensors:
			A: @::FLOOR.sensors.A
			B: @::FLOOR.sensors.B
			C: (p, o) -> o.vd p.fx - p.hw, p.fy, p.vl, -1
			D: (p, o) -> o.vd p.fx + p.hw, p.fy, p.vl, -1
			E: (p, o) -> o.hd p.fx, p.fy, p.hl, 1
			G: (p, o) -> o.hd p.fx, p.fy, p.hl, -1
			# These sensors are used to enable landing on steep slopes
			I: (p, o) -> o.hd p.fx, p.fy + p.hw, p.vl, 1
			J: (p, o) -> o.hd p.fx, p.fy - p.hw, p.vl, 1
			K: (p, o) -> o.hd p.fx, p.fy + p.hw, p.vl, -1
			L: (p, o) -> o.hd p.fx, p.fy - p.hw, p.vl, -1
	
	constructor: ->
		# Set initial player attributes
		@ringCount = 0
		@emeralds = []
		@score = 0
		# The horizontal/vertical offsets of the sensors
		@hw = Math.floor(@width / 2 - 1)
		# The invincibility from monitors
		@invincTime = 0
		# The invincibility from damage
		@tmpInvincTime = 0
		@createAnimations()
		@setInitialState()
		
	createAnimations: ->
		@animations =
			walking: new sonickit.Animation(@walkingAnimFrames, 8)
			running: new sonickit.Animation(@runningAnimFrames, 8)
			spinning: new sonickit.Animation(@spinningAnimFrames, 8)
			standing: new sonickit.Animation(@standingAnimFrames, -1)
		@currentAnimation = @animations.walking
	
	draw: (ctx, camera, remTime) ->
		# Interpolate the position and angle
		ix = @oldX * (1 - remTime) + @x * remTime
		iy = @oldY * (1 - remTime) + @y * remTime
		#iangle = @oldAngle * (1 - remTime) + @angle * remTime
		img = @currentAnimation.getCurrentImage()
		ctx.save()
		ctx.translate ix - camera.x, iy - camera.y
		ctx.scale -1, 1 if @facingLeft
		ctx.rotate @angle unless @spinning
		@currentAnimation.draw ctx, -img.width / 2, -img.height / 2
		ctx.restore()
	
	# Resets the player's movement parameters to what will usually make sense at the beginning of a level.
	setInitialState: ->
		# Movement parameters
		@angle = 0
		@facingLeft = no
		@quadrant = @AIR
		@airborne = yes
		@spinning = no
		@crouching = no
		@lookingUp = no
		@speed = 0
		@xSpeed = 0
		@ySpeed = 0
		# Number of frames until the horizontal control lock is released (0 if disabled)
		@controlLockTime = 0
		
		@animFrame = 0
		@anim = @standingAnim
		
	setPos: ({@x, @y, @layer}) ->
		@fx = Math.floor @x
		@fy = Math.floor @y
		
	savePos: ->
		@oldAngle = @angle
		@oldX = @x
		@oldY = @y
	
	# Sets the player's angle and quadrant values according to the given measured angle (which is
	# relative to the sensor angles)
	setAngle: (angle) ->
		if angle > @QUADRANT_SWITCH_THRESHOLD
			angle -= @QUADRANT_SWITCH_ADJUSTMENT
			@quadrant = @[@quadrant.right]
		else if angle < -@QUADRANT_SWITCH_THRESHOLD
			angle += @QUADRANT_SWITCH_ADJUSTMENT
			@quadrant = @[@quadrant.left]
		# Adjust the angle to the current quadrant
		@angle = @quadrant.base + angle
	
	# Moves the player 'shift' pixels away from the ground.
	shiftHeight: (shift) ->
		switch @quadrant
			when @FLOOR
				@y -= shift
			when @LEFT_WALL
				@x += shift
			when @CEILING
				@y += shift
			when @RIGHT_WALL
				@x -= shift
	
	# Adjusts the player's height according to the currently measured distance to the ground.
	# Does nothing if the player is in the air.
	adjustHeight: (dist) ->
		@shiftHeight @height / 2 - dist
	
	# Adjusts the player's position along the ground when colliding with walls.
	# 'left' is true if the collision is with a left wall, false otherwise.
	adjustWidth: (dist, left) ->
		shift = dist - @width / 2
		shift = -shift if left
		switch @quadrant
			when @FLOOR, @AIR
				@x += shift
			when @LEFT_WALL
				@y += shift
			when @CEILING
				@x -= shift
			when @RIGHT_WALL
				@y -= shift
	
	# These functions return true if the player should be considered to have collided with the given object.
	# Used only for objects which affect player physics.
	# @param obj The object that may have been hit.
	# @param dist The distance to that object, as measured by the physics sensors
	shouldHitFloor: (obj, dist) ->
		not @airborne or @ySpeed > 0 and @height / 2 > dist
	
	shouldHitCeiling: (obj, dist) ->
		@ySpeed < 0 and @height / 2 > dist
	
	shouldHitLeftWall: (obj, dist) ->
		(if @airborne then @xSpeed else @speed) < 0
	
	shouldHitRightWall: (obj, dist) ->
		(if @airborne then @xSpeed else @speed) > 0
		
	# Puts the player in the air state, without spinning and mantaining the current speed.
	fall: ->
		@quadrant = @AIR
		@airborne = yes
		@xSpeed = @speed * Math.cos(@angle)
		@ySpeed = @speed * Math.sin(@angle)
	
	# Determines the player's reaction to the environment it has collided with, as measured by the 12
	# possible environment sensors.
	collide: (ah, bh, ch, dh, eh, fh, gh, hh, ih, jh, kh, lh) ->
		dist = sonickit.min ah, bh
		if @airborne
			if (ah? or bh?) and @ySpeed > 0 and @height / 2 > dist
				# Land
				@airborne = no
				@quadrant = @FLOOR
				if ah? and bh?
					@setAngle Math.atan2(bh - ah, 2 * @hw)
				# Handle very steep slopes
				else if ih? and jh?
					@quadrant = @RIGHT_WALL
					@angle = Math.atan2(jh - ih, 2 * @hw) + @RIGHT_WALL.base
					dist = Math.min ih, jh
				else if kh? and lh?
					@quadrant = @LEFT_WALL
					@angle = Math.atan2(lh - kh, 2 * @hw) + @LEFT_WALL.base
					dist = Math.min kh, lh
				else
					@angle = 0
				@adjustHeight dist
				@unroll() if @spinning
				@speed = Math.sqrt(@xSpeed * @xSpeed + @ySpeed * @ySpeed) *
				         Math.cos(Math.atan2(@ySpeed, @xSpeed) - @angle)
			# Wall collision
			if eh? and @xSpeed > 0
				@adjustWidth eh, no
				@xSpeed = 0
			else if gh? and @xSpeed < 0
				@adjustWidth gh, yes
				@xSpeed = 0
		else
			# Handle floor collision
			if ah? and bh?
				@adjustHeight dist
				@setAngle Math.atan2(bh - ah, 2 * @hw)
			# If on the edge of the platform, keep the last angle that was measured
			else if ah?
				@adjustHeight ah
			else if bh?
				@adjustHeight bh
			else
				@fall()
			# Handle wall collision
			if (eh? or fh?) and @speed > 0
				@adjustWidth sonickit.min(eh, fh), no
				@controlLockTime = 1
				@speed = 0
			else if (gh? or hh?) and @speed < 0
				@adjustHeight sonickit.min(gh, hh), yes
				@controlLockTime = 1
				@speed = 0
				
	# The default physics values.
	gravity: 0.21875
	accel: 0.046875
	decel: 0.5
	friction: 0.046875
	slopeFac: 0.125

	airAccel: 0.09375
	# The X speed below which air drag is not applied
	airDragThreshold: 0.125
	airDragFactor: 0.96875

	rollingFriction: 0.023475
	rollingDecel: 0.125

	unrollSpeed: 0.5
	minRollSpeed: 1.03125
	minWallSpeed: 2.5
	speedCap: 6
	ySpeedCap: 16

	jumpSpeed: 6.5
	
	slideTime: 30
	
	applyFriction: ->
		@speed -= Math.min(Math.abs(@speed), (if @spinning then @rollingFriction else @friction)) * sonickit.sign(@speed)
		
	# The speed past which the player enters the running animation.
	runningAnimationThreshold: 6

	# Handles player acceleration and deceleration when walking
	moveWalk: (kb) ->
		@currentAnimation = if Math.abs(@speed) < @runningAnimationThreshold then @animations.walking else @animations.running
		if kb[ctl.left]?
			if @speed <= 0
				if @speed > -@speedCap
					@speed -= @accel
					@speed = -@speedCap if @speed < -@speedCap
			else
				@speed -= @decel
			if @speed < 0
				@facingLeft = yes
		else if kb[ctl.right]?
			if @speed >= 0
				if @speed < @speedCap
					@speed += @accel
					@speed = @speedCap if @speed > @speedCap
			else
				@speed += @decel
			if @speed > 0
				@facingLeft = no
		else
			@applyFriction()

	moveRoll: (kb) ->
		if @speed > 0 and kb[ctl.left]?
			@speed -= @rollingDecel
		else if @speed < 0 and kb[ctl.right]?
			@speed += @rollingDecel
		@applyFriction()
		# Unroll if going too slowly.
		if Math.abs(@speed) < @unrollSpeed
			@unroll()

	moveAir: (kb) ->
		if -4 < @ySpeed < 0 and Math.abs(@xSpeed) >= @airDragThreshold
			@xSpeed *= @airDragFactor
		@ySpeed += @gravity
		@ySpeed = @ySpeedCap if @ySpeed > @ySpeedCap
		@x += @xSpeed
		@y += @ySpeed
		if kb[ctl.left]? and @xSpeed > -@speedCap
			@xSpeed -= @airAccel
			@xSpeed = -@speedCap if @xSpeed < -@speedCap
			@facingLeft = true
		else if kb[ctl.right]? and @xSpeed < @speedCap
			@xSpeed += @airAccel
			@xSpeed = @speedCap if @xSpeed > @speedCap
			@facingLeft = false
	
	# Causes the player to jump (in a spinning state).
	jump: ->
		@roll() unless @spinning
		@airborne = yes
		@quadrant = @AIR
		@xSpeed = @speed * Math.cos(@angle) + @jumpSpeed * Math.sin(@angle)
		@ySpeed = @speed * Math.sin(@angle) - @jumpSpeed * Math.cos(@angle)
	
	# Makes the player shrink by @rollHeightDiff pixels towards the ground.
	shrinkY: ->
		@height -= @rollHeightDiff
		@shiftHeight -@rollHeightDiff / 2
	
	# Reverses the action of shrinkY.
	growY: ->
		@height += @rollHeightDiff
		@shiftHeight @rollHeightDiff / 2
	
	# Causes the player to roll into a ball (also lowers its height, like crouching)
	roll: ->
		@currentAnimation = @animations.spinning
		@spinning = yes
		# Ensure that the player's height is not reduced again when going from crouching to spinning
		# (the opposite isn't a problem because the player unrolls first)
		if @crouching
			@crouching = no
			return
		@shrinkY()
	
	unroll: ->
		@spinning = no
		@growY()

	crouch: ->
		@crouching = yes
		@shrinkY()
		@speed = 0

	uncrouch: ->
		@crouching = no
		@growY()
	
	# Calculates the player's X and Y pixel coordinates (used for collision). They are stored
	# in this.fx and this.fy.
	calcIntPos: ->
		@fx = Math.floor @x
		@fy = Math.floor @y
	
	update: ->
		@oldX = @x
		@oldY = @y
		kb = @level.runner.keyboard.keyTable
		@currentAnimation.interval = Math.max(8 - Math.floor(Math.abs(@speed)), 1)
		@currentAnimation.update()
		if @airborne
			@moveAir kb
		else
			@x += @speed * Math.cos(@angle)
			@y += @speed * Math.sin(@angle)
			# Handle player acceleration and deceleration
			if @controlLockTime is 0
				if @spinning
					@moveRoll kb
				else unless @crouching or @lookingUp
					@moveWalk kb
			else
				@controlLockTime--
				@applyFriction()
			@speed += @slopeFac * Math.sin(@angle)
			# Fall off of walls and ceilings when going too slowly
			if @quadrant isnt @FLOOR and Math.abs(@speed) < @minWallSpeed
				@fall()
				@controlLockTime = @slideTime
				return
			if @crouching and not kb[ctl.down]?
				@uncrouch()
			if @speed is 0
				@currentAnimation = @animations.standing
			# Rolling
			if kb[ctl.down]? and not @spinning
				if Math.abs(@speed) >= @minRollSpeed
					return @roll()
				else unless @crouching
					return @crouch()
			# Jumping (only if the jump button has just been pressed)
			if kb[ctl.jump] is 1
				return @jump()
	
	getRect: ->
		x: Math.floor(@x - @width / 2)
		y: Math.floor(@y - @height / 2)
		width: @width
		height: @height
