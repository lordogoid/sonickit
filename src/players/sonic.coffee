frameseq = sonickit.Animation.genFrameSequence

class sonickit.players.Sonic extends sonickit.Player
	walkingAnimFrames: frameseq "art/sonic/walk/$.png", 8
	runningAnimFrames: frameseq "art/sonic/run/$.png", 4
	spinningAnimFrames: frameseq "art/sonic/spin/$.png", 4
	standingAnimFrames: ["art/sonic/stand.png"]