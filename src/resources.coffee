# This object is responsible for loading resources (images and music) asynchronously.
sonickit.Resources =
	# Used for resources that are used everywhere
	global:
		music: {}
		images: {}
	# Used for resources specific to a level or screen
	specific:
		music: {}
		images: {}
	
	load: (target, resList, onload) ->
		# The number of resources to load. This is decremented each time one loads,
		# and when it reaches 0, the onload handler that was passed in is called
		rsrcCount = resList.music.length + resList.images.length
		innerOnload = ->
			onload() if --rsrcCount is 0
		# Load images
		target.images = {}
		for name in resList.images
			img = new Image
			img.addEventListener 'load', innerOnload
			img.src = name
			target.images[name] = img
		# Load music
		target.music = {}
		for name in resList.music
			obj = new Audio
			obj.addEventListener 'canplaythrough', innerOnload
			obj.src = name
			target.music[name] = obj
		null
	
	loadGlobal: (resList, onload) ->
		@load @global, resList, onload
	
	loadSpecific: (resList, onload) ->
		@load @specific, resList, onload
	
	getImage: (name) ->
		@specific.images[name] or @global.images[name]
	
	getMusic: (name) ->
		@specific.music[name] or @global.music[name]