# Returns the current timestamp (relative to an arbitrary time) in milliseconds, with at least millisecond precision
# (better if available)
sonickit.now = do ->
	perf = performance
	f = perf.now or perf.webkitNow or perf.mozNow or perf.msNow or perf.oNow
	if f?
		-> f.call perf
	else
		-> Date.now()

# This class is responsible for deciding which level/screen of the game is currently running, and periodically telling it to update.
# Visually, the game will always run at the same speed, no matter what the display
# framerate is, and the physics will behave identically. However, at very low
# framerates, there may be some input lag due to JavaScript event handlers
# being unable to interrupt other code.
class sonickit.Runner
	UPDATE_INTERVAL: 16
	PHYSICS_INTERVAL: 1000 / 60
	
	# Creates a Runner object that draws the game on the given canvas.
	constructor: (canvas) ->
		@ctx = canvas.getContext '2d'
		@keyboard = new sonickit.Keyboard(canvas)
	
	# Starts updating the game's status periodically. Does nothing if the game is already running.
	run: ->
		return if @intervalID?
		accumTime = 0
		prev = sonickit.now()
		@intervalID = setInterval =>
			cur = sonickit.now()
			accumTime += cur - prev
			prev = cur
			# Run the physics simulation until it is on par with or ahead of the display
			while accumTime > 0
				@keyboard.update()
				@curSection.update()
				@keyboard.delPressedKeys()
				accumTime -= @PHYSICS_INTERVAL
			@curSection.draw @ctx, accumTime / @PHYSICS_INTERVAL + 1
		, @UPDATE_INTERVAL
		
	setCurSection: (@curSection) ->
		@curSection.runner = this
	
	# Stops updating the game until #run is called again.
	stop: ->
		clearInterval @intervalID
		@intervalID = null
