class window.FlatPlaf extends sonickit.Object
	collisionMaskName: 'flat_plaf.png'
	defaultAnim: ['flat_plaf.png']
	topSolid: yes
	
class window.SlantedPlaf extends sonickit.Object
	collisionMaskName: 'slanted_plaf.png'
	defaultAnim: ['slanted_plaf.png']
	topSolid: yes
	rightSolid: yes
	
class window.QuarterPipe extends sonickit.Object
	collisionMaskName: 'quarter_pipe2.png'
	defaultAnim: ['quarter_pipe2.png']
	topSolid: yes
	rightSolid: yes

class window.SlopedPlaf extends sonickit.Object
	collisionMaskName: 'sloped_plaf.png'
	defaultAnim: ['sloped_plaf.png']
	topSolid: yes
	rightSolid: yes

window.TestLevel = new sonickit.Level(1600, 800)

window.player = new sonickit.players.Sonic

TestLevel.spawnPositions =
	default:
		x: 50
		y: 50
		layer: 1

TestLevel.populate = ->
	@add(new QuarterPipe(0, 170, 1))

TestLevel.rsrcList =
	images: ['flat_plaf.png', 'slanted_plaf.png', 'quarter_pipe2.png', 'sloped_plaf.png']
	music: []
	
frameseq = sonickit.Animation.genFrameSequence

S = sonickit.players.Sonic

globalResourceList =
	images: S::walkingAnimFrames.concat S::runningAnimFrames.concat S::spinningAnimFrames.concat S::standingAnimFrames
	music: []

sonickit.Resources.loadSpecific TestLevel.rsrcList, ->
	TestLevel.populate()
TestLevel.placePlayer player

window.onload = ->
	runner = new sonickit.Runner(document.getElementById('game'))
	runner.setCurSection TestLevel
	sonickit.Resources.loadGlobal globalResourceList, ->
		console.log "All set!"
		runner.run()